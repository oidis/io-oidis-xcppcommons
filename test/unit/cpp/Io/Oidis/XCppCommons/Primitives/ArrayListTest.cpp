/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::XCppCommons::Primitives {
    TEST(ArrayList, Join) {
        ASSERT_STREQ("hello awesome world", ArrayList::Join({"hello", "awesome", "world"}).c_str());
        ASSERT_STREQ("some;words;to;join", ArrayList::Join({"some", "words", "to", "join"}, ";").c_str());
    }
}
