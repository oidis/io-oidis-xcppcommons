/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_APPLICATION_HPP_
#define IO_OIDIS_XCPPCOMMONS_APPLICATION_HPP_

namespace Io::Oidis::XCppCommons {
    /**
     * This class contains base application logic.
     */
    class Application {
     public:
        /**
         * Method for main application logic.
         * @param $argc An argument count.
         * @param $argv An array of arguments.
         */
        virtual int Run(const int $argc, const char *$argv[]);
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_APPLICATION_HPP_
