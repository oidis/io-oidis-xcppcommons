/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_PRIMITIVES_BASEOBJECT_HPP_
#define IO_OIDIS_XCPPCOMMONS_PRIMITIVES_BASEOBJECT_HPP_

namespace Io::Oidis::XCppCommons::Primitives {
    /**
     * BaseObject class implements IBaseObject interface and from this base should derive all classes.
     */
    class BaseObject : public Io::Oidis::XCppCommons::Interfaces::IBaseObject {
     public:
        virtual string ToString() const;

        friend std::ostream &operator<<(std::ostream &$os, const BaseObject &$object);
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_PRIMITIVES_BASEOBJECT_HPP_
