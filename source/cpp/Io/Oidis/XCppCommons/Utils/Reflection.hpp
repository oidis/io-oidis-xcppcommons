/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_UTILS_REFLECTION_HPP_
#define IO_OIDIS_XCPPCOMMONS_UTILS_REFLECTION_HPP_

namespace Io::Oidis::XCppCommons::Utils {
    /**
     * Reflection class provides API to invoke method by name and other features limited to principles of static reflection.
     */
    class Reflection {
     public:
        /**
         * Invoke method. Currently limited to Loader::Run() method.
         * @param $method Define method name with whole namespace.
         * @param $argc !--Argument--!
         * @param $argv !--Argument--!
         * @return Returns !--RetVal--!
         */
        static int Invoke(const string &$method, const int $argc, const char **$argv);
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_UTILS_REFLECTION_HPP_
