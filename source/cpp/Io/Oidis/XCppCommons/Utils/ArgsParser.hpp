/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_UTILS_ARGSPARSER_HPP_
#define IO_OIDIS_XCPPCOMMONS_UTILS_ARGSPARSER_HPP_

namespace Io::Oidis::XCppCommons::Utils {
    /**
     * ArgsParser class provides static functions for program arguments parsing.
     */
    class ArgsParser : private Io::Oidis::XCppCommons::Interfaces::INonCopyable,
                       private Io::Oidis::XCppCommons::Interfaces::INonMovable {
     public:
        /**
         * @param $args [out] Specify instance of ProgramArgs to holds parsed options.
         * @param $argc Specify count of command line arguments count.
         * @param $argv Specify array of arguments strings.
         * @return Returns 0 if succeed, 1 if print help or version has been done, -1 if error occurred.
         */
        static int Parse(Io::Oidis::XCppCommons::Structures::ProgramArgs &$args,
                         const int $argc, const char **$argv);
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_UTILS_ARGSPARSER_HPP_
