/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_IOAPI_IOHANDLERFACTORY_HPP_
#define IO_OIDIS_XCPPCOMMONS_IOAPI_IOHANDLERFACTORY_HPP_

namespace Io::Oidis::XCppCommons::IOApi {
    /**
     * IOHandlerFactory class provides factory for input and output resource handlers.
     */
    class IOHandlerFactory {
        typedef Io::Oidis::XCppCommons::Interfaces::IOHandler IOHandler;

     public:
        /**
         * @param $handlerType Specify IO handler type.
         * @param $owner Sets handler owner.
         * @return Returns instance of specified IO handler or newly created one.
         */
        static shared_ptr<IOHandler> getHandler(const Io::Oidis::XCppCommons::Enums::IOHandlerType $handlerType,
                                        const string &$owner = "");

        /**
         * @param $handler Specify handler to check type.
         * @return Returns type of handler instance.
         */
        static Io::Oidis::XCppCommons::Enums::IOHandlerType getHandlerType(const shared_ptr<IOHandler> &$handler);

        /**
         * Destroys all available instances of IO handlers.
         */
        static void DestroyAll();

     private:
        static string handlerIdGenerator(const Io::Oidis::XCppCommons::Enums::IOHandlerType $handlerType, const string &$owner);
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_IOAPI_IOHANDLERFACTORY_HPP_
