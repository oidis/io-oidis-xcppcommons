/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_SYSTEM_ENVIRONMENT_HPP_
#define IO_OIDIS_XCPPCOMMONS_SYSTEM_ENVIRONMENT_HPP_

namespace Io::Oidis::XCppCommons::System {
    /**
     * Environment class provides API to control or use current system environment features.
     */
    class Environment {
     public:
        /**
         * Expands given string with symbols from current process environment variables.
         * @param $data Specify input string.
         * @return Returns expanded string or original if no environment variable symbol found.
         */
        static string Expand(const string &$data);
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_SYSTEM_ENVIRONMENT_HPP_
