/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_SYSTEM_PROCESS_DETAIL_INITIALIZERPOSIX_HPP_
#define IO_OIDIS_XCPPCOMMONS_SYSTEM_PROCESS_DETAIL_INITIALIZERPOSIX_HPP_

#if defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)

#include <boost/process/detail/posix/handler.hpp>

#include "../../../Utils/LogIt.hpp"

namespace Io::Oidis::XCppCommons::System::Process::Detail {
    /**
     * TerminalProcessInitializer provides custom initialization of executor from boost::process for POSIXes.
     */
    struct Initializer : ::boost::process::detail::posix::handler_base_ext {
        template<typename PosixExecutor>
        void on_setup(PosixExecutor &$executor) const {
            Io::Oidis::XCppCommons::Utils::LogIt::Debug("Setting up POSIX executor with PID {0} and executable {1}",
                                                                  std::to_string($executor.pid), $executor.exe);
        }

        template<typename PosixExecutor>
        void on_success(PosixExecutor &$executor) const {
            Io::Oidis::XCppCommons::Utils::LogIt::Info("POSIX process {0} successfully launched with PID {1}",
                                                                 $executor.exe, std::to_string($executor.pid));
        }

        template<typename PosixExecutor>
        void on_error(PosixExecutor &$executor, const std::error_code &$error) const {
            Io::Oidis::XCppCommons::Utils::LogIt::Error("POSIX process {0} not launched due to error: {1}",
                                                                  $executor.exe, $error.message());
        }

        template<typename PosixExecutor>
        void on_fork_error(PosixExecutor &$executor, const std::error_code &$error) const {
            Io::Oidis::XCppCommons::Utils::LogIt::Error("Error when forking POSIX executor with PID {0}, executable {1}: {2}",
                                                                  std::to_string($executor.pid), $executor.exe, $error.message());
        }

        template<typename PosixExecutor>
        void on_exec_setup(PosixExecutor &$executor) const {
            Io::Oidis::XCppCommons::Utils::LogIt::Debug("Setting up POSIX executor for exec call with PID {0} and executable {1}",
                                                                  std::to_string($executor.pid), $executor.exe);
        }

        template<typename PosixExecutor>
        void on_exec_error(PosixExecutor &$executor, const std::error_code &$error) const {
            Io::Oidis::XCppCommons::Utils::LogIt::Error("Error during exec on POSIX executor with PID {0}, executable {1}: {2}",
                                                                  std::to_string($executor.pid), $executor.exe, $error.message());
        }
    };
}

#endif  // defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)

#endif  // IO_OIDIS_XCPPCOMMONS_SYSTEM_PROCESS_DETAIL_INITIALIZERPOSIX_HPP_
