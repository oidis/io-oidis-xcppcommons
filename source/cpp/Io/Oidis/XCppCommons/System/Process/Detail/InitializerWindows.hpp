/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_SYSTEM_PROCESS_DETAIL_INITIALIZERWINDOWS_HPP_
#define IO_OIDIS_XCPPCOMMONS_SYSTEM_PROCESS_DETAIL_INITIALIZERWINDOWS_HPP_

#ifdef WIN_PLATFORM

#include <boost/process/detail/handler.hpp>

#include "../../../Utils/LogIt.hpp"

namespace Io::Oidis::XCppCommons::System::Process::Detail {
    /**
     * TerminalProcessInitializer provides custom initialization of executor from boost::process for Windows.
     */
    struct Initializer : ::boost::process::detail::handler_base {
        template<typename WindowsExecutor>
        void on_setup(WindowsExecutor &$executor) const {
            Io::Oidis::XCppCommons::Utils::LogIt::Debug("Setting up Windows executor with PID {0} and executable {1}",
                                                                  std::to_string($executor.proc_info.dwProcessId),
                                                                  getExecutorExecutable($executor));
            $executor.creation_flags = CREATE_NO_WINDOW;
        }

        template<typename WindowsExecutor>
        void on_success(WindowsExecutor &$executor) const {
            Io::Oidis::XCppCommons::Utils::LogIt::Info("Windows process {0} successfully launched with PID {1}",
                                                                 getExecutorExecutable($executor),
                                                                 std::to_string($executor.proc_info.dwProcessId));
        }

        template<typename WindowsExecutor>
        void on_error(WindowsExecutor &$executor, const std::error_code &$error) const {
            Io::Oidis::XCppCommons::Utils::LogIt::Error("Windows process {0} not launched due to error: {1}",
                                                                  getExecutorExecutable($executor), $error.message());
        }

        template<typename WindowsExecutor>
        std::string getExecutorExecutable(const WindowsExecutor &$executor) const {
            return $executor.exe != nullptr ? $executor.exe : "(unknown)";
        }
    };
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_XCPPCOMMONS_SYSTEM_PROCESS_DETAIL_INITIALIZERWINDOWS_HPP_
