/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Io::Oidis::XCppCommons::System::Process {
    using Io::Oidis::XCppCommons::Utils::LogIt;

    bool TaskManager::Terminate(const TaskInfo &$info) {
        const bool status = Terminate($info.getPid());

        LogIt::Info("Task \"{0}\" termination status {1}.", $info, status);

        return status;
    }
}
