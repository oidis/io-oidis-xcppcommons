/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_INTERFACES_IOHANDLER_HPP_
#define IO_OIDIS_XCPPCOMMONS_INTERFACES_IOHANDLER_HPP_

namespace Io::Oidis::XCppCommons::Interfaces {
    /**
     * IOHandler class provides interface definition for each input or output handlers.
     */
    class IOHandler {
     public:
        /**
         * Interface to initialize handler instance.
         */
        virtual void Init() = 0;

        /**
         * @return Returns handler name.
         */
        virtual string Name() = 0;

        /**
         * @return Returns handler encoding.
         */
        virtual string Encoding() = 0;

        /**
         * @return Returns handler new line string.
         */
        virtual string NewLine() = 0;

        /**
         * @param $message Message to be printed.
         */
        virtual void Print(const string &$message) = 0;

        /**
         * @param $message Message to be printed with new line appended.
         */
        virtual void Println(const string &$message) = 0;
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_INTERFACES_IOHANDLER_HPP_
