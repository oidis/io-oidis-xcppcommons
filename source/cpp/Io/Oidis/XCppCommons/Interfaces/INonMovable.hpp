/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_INTERFACES_INONMOVABLE_HPP_
#define IO_OIDIS_XCPPCOMMONS_INTERFACES_INONMOVABLE_HPP_

namespace Io::Oidis::XCppCommons::Interfaces {
    /**
     * This interface class defines NonMovable in derived classes.
     */
    class INonMovable {
     protected:
        INonMovable() = delete;  // NOLINT

        ~INonMovable() = delete; // NOLINT

     private:
        INonMovable(const INonMovable &&) = delete; // NOLINT

        INonMovable &operator=(const INonMovable &&) = delete; // NOLINT
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_INTERFACES_INONMOVABLE_HPP_
