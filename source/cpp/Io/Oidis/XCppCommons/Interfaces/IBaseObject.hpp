/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_INTERFACES_IBASEOBJECT_HPP_
#define IO_OIDIS_XCPPCOMMONS_INTERFACES_IBASEOBJECT_HPP_

namespace Io::Oidis::XCppCommons::Interfaces {
    /**
     * Interface for each objects in XCpp projects in WUI Framework.
     */
    class IBaseObject {
     public:
        /**
         * @return Returns object converted to string.
         */
        virtual string ToString() const = 0;
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_INTERFACES_IBASEOBJECT_HPP_
