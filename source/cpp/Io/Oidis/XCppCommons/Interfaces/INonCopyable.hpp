/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_INTERFACES_INONCOPYABLE_HPP_
#define IO_OIDIS_XCPPCOMMONS_INTERFACES_INONCOPYABLE_HPP_

namespace Io::Oidis::XCppCommons::Interfaces {
    /**
     * This interface class defines NonCopyable in derived classes.
     */
    class INonCopyable {
     protected:
        INonCopyable() = delete;  // NOLINT

        ~INonCopyable() = delete;  // NOLINT

     private:
        INonCopyable(const INonCopyable &) = delete;  // NOLINT

        INonCopyable &operator=(const INonCopyable &) = delete;  // NOLINT
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_INTERFACES_INONCOPYABLE_HPP_
