/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "sourceFilesMap.hpp"

using Io::Oidis::XCppCommons::Application;

using Io::Oidis::XCppCommons::Utils::LogIt;
using Io::Oidis::XCppCommons::Enums::LogLevel;
using Io::Oidis::XCppCommons::Enums::IOHandlerType;
using Io::Oidis::XCppCommons::IOApi::IOHandlerFactory;
using Io::Oidis::XCppCommons::EnvironmentArgs;
using Io::Oidis::XCppCommons::Structures::ProgramArgs;
using Io::Oidis::XCppCommons::Utils::ArgsParser;

int Application::Run(const int $argc, const char **$argv) {
    ProgramArgs ba;

    if (ArgsParser::Parse(ba, $argc, $argv) == 0) {
        LogIt::Info("This is Io::Oidis::XCppCommons library");
        std::cout << "This is Io::Oidis::XCppCommons library" << std::endl;
    }
    return 0;
}
