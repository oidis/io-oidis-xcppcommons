/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Io::Oidis::XCppCommons::Primitives::BaseEnum;
using Io::Oidis::XCppCommons::Enums::HttpMethodType;

namespace Io::Oidis::XCppCommons::Enums {
    WUI_ENUM_IMPLEMENT(HttpMethodType);

    WUI_ENUM_CONST_IMPLEMENT(HttpMethodType, GET);
    WUI_ENUM_CONST_IMPLEMENT(HttpMethodType, HEAD);
    WUI_ENUM_CONST_IMPLEMENT(HttpMethodType, POST);
    WUI_ENUM_CONST_IMPLEMENT(HttpMethodType, TRACE);
    WUI_ENUM_CONST_IMPLEMENT(HttpMethodType, WSS);
    WUI_ENUM_CONST_IMPLEMENT(HttpMethodType, DATA);
}
