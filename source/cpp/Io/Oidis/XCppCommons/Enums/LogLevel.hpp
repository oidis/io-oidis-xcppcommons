/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_ENUMS_LOGLEVEL_HPP_
#define IO_OIDIS_XCPPCOMMONS_ENUMS_LOGLEVEL_HPP_

#ifdef ERROR
#undef ERROR
#endif

namespace Io::Oidis::XCppCommons::Enums {
    /**
     * LogLevel enum provides definition of log levels.
     */
    class LogLevel
            : public Io::Oidis::XCppCommons::Primitives::BaseEnum<Io::Oidis::XCppCommons::Enums::LogLevel> {
     WUI_ENUM_DECLARE(LogLevel)

     public:
        static const LogLevel ALL;
        static const LogLevel INFO;
        static const LogLevel WARNING;
        static const LogLevel ERROR;
        static const LogLevel DEBUG;
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_ENUMS_LOGLEVEL_HPP_
