/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_ENUMS_HTTPMETHODTYPE_HPP_
#define IO_OIDIS_XCPPCOMMONS_ENUMS_HTTPMETHODTYPE_HPP_

namespace Io::Oidis::XCppCommons::Enums {
    /**
     * HttpMethodType enum provides definition of HTTP methods.
     */
    class HttpMethodType
            : public Io::Oidis::XCppCommons::Primitives::BaseEnum<Io::Oidis::XCppCommons::Enums::HttpMethodType> {
     WUI_ENUM_DECLARE(HttpMethodType)

     public:
        static const HttpMethodType GET;
        static const HttpMethodType HEAD;
        static const HttpMethodType POST;
        static const HttpMethodType TRACE;
        static const HttpMethodType WSS;
        static const HttpMethodType DATA;
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_ENUMS_HTTPMETHODTYPE_HPP_
