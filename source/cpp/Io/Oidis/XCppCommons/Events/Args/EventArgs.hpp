/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_EVENTS_ARGS_EVENTARGS_HPP_
#define IO_OIDIS_XCPPCOMMONS_EVENTS_ARGS_EVENTARGS_HPP_

namespace Io::Oidis::XCppCommons::Events::Args {
    /**
     * EventArgs class provides basic event structure.
     */
    class EventArgs : public Io::Oidis::XCppCommons::Primitives::BaseObject {
     public:
        /**
         * Construct args with itself owner.
         */
        EventArgs();

        /**
         * Construct args with specified owner.
         * @param $owner A pointer to owner.
         */
        explicit EventArgs(void *$owner);

        /**
         * Specified object, which own current args.
         * @return Returns current event args owner object.
         */
        void *getOwner() const;

     private:
        void *owner;
    };
}

#endif  // IO_OIDIS_XCPPCOMMONS_EVENTS_ARGS_EVENTARGS_HPP_
