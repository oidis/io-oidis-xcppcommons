/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_XCPPCOMMONS_HPP_  // NOLINT
#define IO_OIDIS_XCPPCOMMONS_HPP_

#ifdef _WIN32
#define BOOST_WINAPI_IS_MINGW_W64
#endif

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/any.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/unordered_map.hpp>
#include <boost/assign.hpp>
#include <boost/regex.hpp>
#include <boost/interprocess/ipc/message_queue.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/thread.hpp>
#include <boost/thread/barrier.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/iterator/iterator_adaptor.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/format.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/locale/encoding_utf.hpp>
#include <boost/process.hpp>

#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/md5.h>

#include <json.hpp>
#include <promiseAdv.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <cstdlib>
#include <unordered_map>
#include <functional>

// global-using-start
using std::string;
using std::function;
using std::shared_ptr;
using std::unique_ptr;
using std::weak_ptr;
using boost::any;
using nlohmann::json;
// global-using-stop

#include "interfacesMap.hpp"
#include "Io/Oidis/XCppCommons/sourceFilesMap.hpp"

#include "Io/Oidis/XCppCommons/Interfaces/IBaseObject.hpp"
#include "Io/Oidis/XCppCommons/Interfaces/INonCopyable.hpp"
#include "Io/Oidis/XCppCommons/Interfaces/INonMovable.hpp"
#include "Io/Oidis/XCppCommons/Primitives/BaseObject.hpp"
#include "Io/Oidis/XCppCommons/System/IO/FileSystem.hpp"
#include "Io/Oidis/XCppCommons/Primitives/BaseEnum.hpp"
#include "Io/Oidis/XCppCommons/Enums/HttpMethodType.hpp"
#include "Io/Oidis/XCppCommons/Primitives/String.hpp"
#include "Io/Oidis/XCppCommons/Primitives/ArrayList.hpp"
#include "Io/Oidis/XCppCommons/Structures/ProgramArgs.hpp"
#include "Io/Oidis/XCppCommons/Enums/IOHandlerType.hpp"
#include "Io/Oidis/XCppCommons/Enums/LogLevel.hpp"
#include "Io/Oidis/XCppCommons/Interfaces/IOHandler.hpp"
#include "Io/Oidis/XCppCommons/IOApi/Handlers/BaseOutputHandler.hpp"
#include "Io/Oidis/XCppCommons/IOApi/Handlers/ConsoleHandler.hpp"
#include "Io/Oidis/XCppCommons/IOApi/Handlers/OutputFileHandler.hpp"
#include "Io/Oidis/XCppCommons/Utils/LogIt.hpp"
#include "Io/Oidis/XCppCommons/System/Environment.hpp"
#include "Io/Oidis/XCppCommons/Utils/JSON.hpp"
#include "Io/Oidis/XCppCommons/EnvironmentArgs.hpp"
#include "Io/Oidis/XCppCommons/Utils/ArgsParser.hpp"
#include "Io/Oidis/XCppCommons/Utils/ObjectDecoder.hpp"
#include "Io/Oidis/XCppCommons/Utils/ObjectEncoder.hpp"
#include "Io/Oidis/XCppCommons/System/Net/Http/ClientRequest.hpp"
#include "Io/Oidis/XCppCommons/System/Net/Http/ClientResponse.hpp"
#include "Io/Oidis/XCppCommons/System/Process/ProcessOptions.hpp"
#include "Io/Oidis/XCppCommons/System/Process/ExecuteOptions.hpp"
#include "Io/Oidis/XCppCommons/System/Process/SpawnOptions.hpp"
#include "Io/Oidis/XCppCommons/System/Net/Url.hpp"

// generated-code-start
#include "Io/Oidis/XCppCommons/Application.hpp"
#include "Io/Oidis/XCppCommons/Enums/HttpClientType.hpp"
#include "Io/Oidis/XCppCommons/Enums/HttpResponseType.hpp"
#include "Io/Oidis/XCppCommons/Events/Args/EventArgs.hpp"
#include "Io/Oidis/XCppCommons/Events/IntervalTimer.hpp"
#include "Io/Oidis/XCppCommons/Events/ThreadPool.hpp"
#include "Io/Oidis/XCppCommons/Interfaces/IConnector.hpp"
#include "Io/Oidis/XCppCommons/Interfaces/IResponse.hpp"
#include "Io/Oidis/XCppCommons/IOApi/IOHandlerFactory.hpp"
#include "Io/Oidis/XCppCommons/Loader.hpp"
#include "Io/Oidis/XCppCommons/Structures/IpcPipe.hpp"
#include "Io/Oidis/XCppCommons/System/IO/FileGlob.hpp"
#include "Io/Oidis/XCppCommons/System/IO/Path.hpp"
#include "Io/Oidis/XCppCommons/System/Net/Http/Client.hpp"
#include "Io/Oidis/XCppCommons/System/Process/Child.hpp"
#include "Io/Oidis/XCppCommons/System/Process/Detail/InitializerPosix.hpp"
#include "Io/Oidis/XCppCommons/System/Process/Detail/InitializerWindows.hpp"
#include "Io/Oidis/XCppCommons/System/Process/TaskInfo.hpp"
#include "Io/Oidis/XCppCommons/System/Process/TaskManager.hpp"
#include "Io/Oidis/XCppCommons/System/Process/Terminal.hpp"
#include "Io/Oidis/XCppCommons/System/Process/TerminalOptions.hpp"
#include "Io/Oidis/XCppCommons/System/ResponseApi/Handlers/BaseResponse.hpp"
#include "Io/Oidis/XCppCommons/System/ResponseApi/Handlers/CallbackResponse.hpp"
#include "Io/Oidis/XCppCommons/System/ResponseApi/ResponseFactory.hpp"
#include "Io/Oidis/XCppCommons/Utils/Convert.hpp"
#include "Io/Oidis/XCppCommons/Utils/IpcPipeObserver.hpp"
#include "Io/Oidis/XCppCommons/Utils/ObjectValidator.hpp"
#include "Io/Oidis/XCppCommons/Utils/Reflection.hpp"
// generated-code-end


#endif  // IO_OIDIS_XCPPCOMMONS_HPP_  NOLINT
